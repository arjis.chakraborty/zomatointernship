package com.internship.zomato.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.internship.zomato.R;

import java.util.ArrayList;

public class FiltersRecyclerAdapter extends RecyclerView.Adapter<FiltersRecyclerAdapter.ViewHolder> {
    ArrayList<String> models;
    Context context;
    
    public FiltersRecyclerAdapter(ArrayList<String> models, Context context) {
        this.models = models;
        this.context = context;
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filters, parent, false));
    }
    
    @Override
    public int getItemCount() {
        return models.size();
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.text.setText(models.get(position));
    }
    
    class ViewHolder extends RecyclerView.ViewHolder{
        TextView text;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            
            text = itemView.findViewById(R.id.filterName);
        }
    }
}
