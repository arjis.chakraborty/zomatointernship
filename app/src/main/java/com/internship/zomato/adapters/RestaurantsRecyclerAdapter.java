package com.internship.zomato.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.internship.zomato.GrayscaleTransformation;
import com.internship.zomato.R;
import com.internship.zomato.models.RestaurantModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RestaurantsRecyclerAdapter extends RecyclerView.Adapter<RestaurantsRecyclerAdapter.ViewHolder> {
    ArrayList<RestaurantModel> models;
    Context context;
    
    public RestaurantsRecyclerAdapter(ArrayList<RestaurantModel> models, Context context) {
        this.models = models;
        this.context = context;
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_restaurants, parent, false));
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RestaurantModel data = models.get(position);
        holder.name.setText(data.getName());
        holder.discount.setText(data.getDiscount() + "% OFF upto ₹1000");
        holder.growth.setText(data.getGrowth() + "+ people ordered from here since lockdown");
        holder.cost.setText("₹" + data.getCost() + " for one");
        holder.time.setText(data.getTime());
        holder.rating.setText(Html.fromHtml("<p style=\"display:inline;\" > <b>" + data.getRating() + "</b> <span style=\"color:grey; display:inline;\"><small>/5<small></span> </p>"));
        holder.type.setText(data.getType());
        if (!data.getStatus().equals("Open")) {
            holder.status.setText(data.getStatus());
            holder.star.setImageTintList(ColorStateList.valueOf(Color.DKGRAY));
            holder.name.setTextColor(Color.DKGRAY);
            Picasso picasso = Picasso.get();
            picasso.load(data.getPicURL())
                    .transform(new GrayscaleTransformation(picasso))
                    .resize(250, 250)
                    .centerCrop()
                    .placeholder(R.drawable.image_placeholder)
                    .into(holder.pic);
            
        } else {
            holder.status.setVisibility(View.GONE);
            Picasso.get()
                    .load(data.getPicURL())
                    .resize(250, 250)
                    .centerCrop()
                    .placeholder(R.drawable.image_placeholder)
                    .into(holder.pic);
        }
    }
    
    @Override
    public int getItemCount() {
        return models.size();
    }
    
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, type, rating, cost, growth, status, time, discount;
        ImageView star, pic;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.restaurantName);
            type = itemView.findViewById(R.id.type);
            rating = itemView.findViewById(R.id.rating);
            cost = itemView.findViewById(R.id.cost);
            growth = itemView.findViewById(R.id.growth);
            status = itemView.findViewById(R.id.restaurant_status);
            time = itemView.findViewById(R.id.time);
            pic = itemView.findViewById(R.id.foodImage);
            discount = itemView.findViewById(R.id.discount);
            star = itemView.findViewById(R.id.star);
        }
    }
}
