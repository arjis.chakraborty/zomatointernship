package com.internship.zomato;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.RelativeLayout;

import com.internship.zomato.adapters.FiltersRecyclerAdapter;
import com.internship.zomato.adapters.RestaurantsRecyclerAdapter;
import com.internship.zomato.models.RestaurantModel;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    //RelativeLayout topLayout;
    
    FiltersRecyclerAdapter filtersRecyclerAdapter;
    RestaurantsRecyclerAdapter restaurantsRecyclerAdapter;
    
    RecyclerView filtersRecycler, restaurantsRecycler;
    
    ArrayList<RestaurantModel> restaurantModels = new ArrayList<>();
    ArrayList<String> filterModels = new ArrayList<>();
    
    NestedScrollView nestedScrollView;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //topLayout = findViewById(R.id.topLayout);
        restaurantsRecycler = findViewById(R.id.mainRecycler);
        filtersRecycler = findViewById(R.id.filterRecycler);
        
        nestedScrollView = findViewById(R.id.nestedScroll);
        
        restaurantsRecycler.setLayoutManager(new LinearLayoutManager(this));
        filtersRecycler.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        
        nestedScrollView.setNestedScrollingEnabled(true);
        restaurantsRecycler.setNestedScrollingEnabled(false);
        
        setRestaurantsData();
        setFilterData();
    }
    
    private void setRestaurantsData() {
        RestaurantModel model1 = new RestaurantModel();
        model1.setName("China Town");
        model1.setCost((new Random().nextInt(500 - 150) + 150) + "");
        model1.setDiscount((new Random().nextInt(50 - 20) + 20) + "");
        model1.setGrowth((new Random().nextInt(10000 - 1500) + 1500) + "");
        model1.setRating(String.format("%.1f", (new Random().nextFloat() * (4.5f - 1.0f) + 1.0f)));
        model1.setStatus("Open");
        model1.setTime("25 mins");
        model1.setType("Chinese, Continental");
        model1.setPicURL("https://www.recipetineats.com/wp-content/uploads/2020/10/General-Tsao-Chicken_1.jpg?resize=650,813");
        
        RestaurantModel model2 = new RestaurantModel();
        model2.setName("Kachori Station");
        model2.setCost((new Random().nextInt(500 - 150) + 150) + "");
        model2.setDiscount((new Random().nextInt(50 - 20) + 20) + "");
        model2.setGrowth((new Random().nextInt(10000 - 1500) + 1500) + "");
        model2.setRating(String.format("%.1f", (new Random().nextFloat() * (4.5f - 1.0f) + 1.0f)));
        model2.setStatus("Open");
        model2.setTime("40 mins");
        model2.setPicURL("https://www.spiceupthecurry.com/wp-content/uploads/2015/08/khasta-kachori-recipe-18.jpg");
        model2.setType("Kachori, Fast Food");
        
        RestaurantModel model3 = new RestaurantModel();
        model3.setName("Momo Plaza");
        model3.setCost((new Random().nextInt(500 - 150) + 150) + "");
        model3.setDiscount((new Random().nextInt(50 - 20) + 20) + "");
        model3.setGrowth((new Random().nextInt(10000 - 1500) + 1500) + "");
        model3.setRating(String.format("%.1f", (new Random().nextFloat() * (4.5f - 1.0f) + 1.0f)));
        model3.setStatus("Open");
        model3.setTime("15 mins");
        model3.setPicURL("https://images.unsplash.com/photo-1534422298391-e4f8c172dddb?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80");
        model3.setType("Momo");
        
        RestaurantModel model4 = new RestaurantModel();
        model4.setName("Hotel Taj");
        model4.setCost((new Random().nextInt(500 - 150) + 150) + "");
        model4.setDiscount((new Random().nextInt(50 - 20) + 20) + "");
        model4.setGrowth((new Random().nextInt(10000 - 1500) + 1500) + "");
        model4.setRating(String.format("%.1f", (new Random().nextFloat() * (4.5f - 1.0f) + 1.0f)));
        model4.setStatus("Open");
        model4.setTime("45 mins");
        model4.setPicURL("https://images.pexels.com/photos/958545/pexels-photo-958545.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
        model4.setType("Indian");
        
        RestaurantModel model5 = new RestaurantModel();
        model5.setName("KFC");
        model5.setCost((new Random().nextInt(500 - 150) + 150) + "");
        model5.setDiscount((new Random().nextInt(50 - 20) + 20) + "");
        model5.setGrowth((new Random().nextInt(10000 - 1500) + 1500) + "");
        model5.setRating(String.format("%.1f", (new Random().nextFloat() * (4.5f - 1.0f) + 1.0f)));
        model5.setStatus("Open");
        model5.setTime("45 mins");
        model5.setPicURL("https://scontent.fccu5-1.fna.fbcdn.net/v/t1.6435-9/67783322_109360193753888_5799094920715173888_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=973b4a&_nc_ohc=Z8UqHuwkY24AX-Hf9Kt&_nc_ht=scontent.fccu5-1.fna&oh=acea3b0d67284d0303f8bd8c423ab221&oe=60A81CCB");
        model5.setType("Fast Food, Fried Chicken");
        
        RestaurantModel model6 = new RestaurantModel();
        model6.setName("KFC");
        model6.setCost((new Random().nextInt(500 - 150) + 150) + "");
        model6.setDiscount((new Random().nextInt(50 - 20) + 20) + "");
        model6.setGrowth((new Random().nextInt(10000 - 1500) + 1500) + "");
        model6.setRating(String.format("%.1f", (new Random().nextFloat() * (4.5f - 1.0f) + 1.0f)));
        model6.setStatus("Opens in 25 minutes");
        model6.setTime("45 mins");
        model6.setPicURL("https://scontent.fccu5-1.fna.fbcdn.net/v/t1.6435-9/67783322_109360193753888_5799094920715173888_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=973b4a&_nc_ohc=Z8UqHuwkY24AX-Hf9Kt&_nc_ht=scontent.fccu5-1.fna&oh=acea3b0d67284d0303f8bd8c423ab221&oe=60A81CCB");
        model6.setType("Fast Food, Fried Chicken");
        
        restaurantModels.add(model1);
        restaurantModels.add(model2);
        restaurantModels.add(model3);
        restaurantModels.add(model4);
        restaurantModels.add(model5);
        restaurantModels.add(model6);
        
        restaurantsRecyclerAdapter = new RestaurantsRecyclerAdapter(restaurantModels, this);
        
        restaurantsRecycler.setAdapter(restaurantsRecyclerAdapter);
    }
    
    private void setFilterData() {
        String model1 = "Pro";
        String model2 = "Cuisines";
        String model3 = "Rating 4.0+";
        String model4 = "MAX Safety";
        String model5 = "Fastest Delivery";
        String model6 = "Offers";
        String model7 = "Takeaway";
        String model8 = "More";
        
        filterModels.add(model1);
        filterModels.add(model2);
        filterModels.add(model3);
        filterModels.add(model4);
        filterModels.add(model5);
        filterModels.add(model6);
        filterModels.add(model7);
        filterModels.add(model8);
        
        filtersRecyclerAdapter = new FiltersRecyclerAdapter(filterModels, this);
        filtersRecycler.setAdapter(filtersRecyclerAdapter);
    }
}